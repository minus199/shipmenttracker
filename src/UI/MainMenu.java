/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import _shippmenttracker.Handler;
import _shippmenttracker.TableViewSample;
import _shippmenttracker._ShippmentTracker;
import _shippmenttracker.eBayHandler.PictureGallery;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javax.swing.table.DefaultTableColumnModel;
import org.w3c.dom.Node;

/**
 *
 * @author minus
 */
public class MainMenu extends Application {
    private Scene scene;
    private FXMLLoader loader;
    private final TableView<MainMenu.MetaInfoModel> table = new TableView<>();
    private MainMenuController controller;

    private ArrayList<ImageView> imageGallery;
    private ObservableList<MetaInfoModel> metaInfoData;

    public MainMenu() throws IOException {
        Handler handler = new Handler("261933387450");
        metaInfoData = FXCollections.observableArrayList(handler.getItem().toMetaInfoModels());
        imageGallery = handler.getPictureGalleryAsImageView();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        controller = new MainMenuController();
        
        scene = new Scene(controller);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Main Menu!");
        createMenu();
        createPictureGallery();
        primaryStage.show();
    }
    
    public void createPictureGallery() {
        Accordion pictureGallery = controller.getPictureGallery();
        
        Iterator imageIterator = imageGallery.iterator();
        int count = 0;
        while (imageIterator.hasNext()) {
            ImageView currentImage = (ImageView) imageIterator.next();
            pictureGallery.getPanes().add(new TitledPane(Integer.toString(count++), currentImage));
        }
        

        pictureGallery.expandedPaneProperty().addListener(
                (ObservableValue<? extends TitledPane> ov, TitledPane old_val,
                        TitledPane new_val) -> {
                    if (new_val != null) {
                        Label label = new Label();
                        label.setText(pictureGallery.getExpandedPane().getText()
                                + ".jpg");
                    }
                });
        
        //controller.getPictureGallery().getPanes().add(new TitledPane("pics", accordion));
        

//        HBox hbox = new HBox(10);
//        hbox.setPadding(new Insets(20, 0, 0, 20));
//        hbox.getChildren().setAll(accordion);
//
//        return hbox;

    }

    public void createMenu() {
        /* Main container */
        //GridPane grid = new GridPane();

        /* Create list for packages */
        ObservableList<String> items = FXCollections.observableArrayList("RS252285556NL", "RS244481196NL", "RS253331196NL", "RS245681196NL");
        controller.getPkgItemList().setItems(items);
        
        TableView metaTable = controller.getMetaInfoTable();
        
        
        metaTable.setItems(metaInfoData);
        
        
//        metaTable.setEditable(true);
//        metaTable.getItems().setAll(metaInfoData);
        
        
        //System.out.println(getController().getTest().getClass());
        //mainMenuController.getPkgItemList().setItems(items);
        
//        ScrollPane pkgList = new ScrollPane();
//        pkgList.setMinHeight(500);
//        pkgList.setContent(controller.getPkgItemList());
//
//        grid.add(pkgList, 1, 1);
//
//        pkgList.setLayoutX(0);
//        pkgList.setLayoutY(0);
               
        
//        HBox gallery = createPictureGallery(imageGallery);
//        gallery.setMaxHeight(500.00);
//
//        grid.add(gallery,2,3);
//
//        grid.setVisible(true);
//        grid.setAlignment(Pos.CENTER);
//        grid.setHgap(20);
//        grid.setVgap(20);
//        grid.setPadding(new Insets(25, 25, 25, 25));

    }

    public static class MetaInfoModel {

        private final String metaName;
        private final String metaValue;

        public MetaInfoModel(String metaName, String metaValue) {
            this.metaName = metaName;
            this.metaValue = metaValue;
        }

        public String getMetaName() {
            return metaName;
        }

        public String getMetaValue() {
            return metaValue;
        }
    }
    

    
    
    
    
    
    
    public void doExample(Stage primaryStage) {
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(20);
        grid.setVgap(20);
        grid.setPadding(new Insets(25, 25, 25, 25));
        grid.setGridLinesVisible(true);

        Text scenetitle = new Text("Welcome");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        grid.add(scenetitle, 0, 0, 2, 1);

        Label userName = new Label("User Name:");
        grid.add(userName, 0, 1);

        TextField userTextField = new TextField();
        grid.add(userTextField, 1, 1);

        Label pw = new Label("Password:");
        grid.add(pw, 0, 2);

        PasswordField pwBox = new PasswordField();
        grid.add(pwBox, 1, 2);

        final Text actiontarget = new Text();
        grid.add(actiontarget, 1, 6);

        Button btn = new Button("Sign in");
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        hbBtn.getChildren().add(btn);
        grid.add(hbBtn, 1, 4);
        btn.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                actiontarget.setFill(Color.FIREBRICK);
                actiontarget.setText("Sign in button pressed");
            }
        });

        Scene scene = new Scene(grid, 500, 400);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Hello World!");
        primaryStage.show();
    }
    
    public GridPane menu2() {
        BorderPane border = new BorderPane();
        HBox hbox = addHBox();
        border.setTop(hbox);
        border.setLeft(addVBox());
        addStackPane(hbox);         // Add stack to HBox in top region

        GridPane gp = addGridPane();
        border.setCenter(gp);
        border.setRight(addFlowPane());

        return gp;
    }
    
    public HBox addHBox() {
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(15, 12, 15, 12));
        hbox.setSpacing(10);
        hbox.setStyle("-fx-background-color: #336699;");

        return hbox;
    }

    public VBox addVBox() {
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(10));
        vbox.setSpacing(8);

        Text title = new Text("Data");
        title.setFont(Font.font("Arial", FontWeight.BOLD, 14));
        vbox.getChildren().add(title);

        Hyperlink options[] = new Hyperlink[]{
            new Hyperlink("Sales"),
            new Hyperlink("Marketing"),
            new Hyperlink("Distribution"),
            new Hyperlink("Costs")};

        for (int i = 0; i < 4; i++) {
            VBox.setMargin(options[i], new Insets(0, 0, 0, 8));
            vbox.getChildren().add(options[i]);
        }

        return vbox;
    }

    public void addStackPane(HBox hb) {
        StackPane stack = new StackPane();
        Rectangle helpIcon = new Rectangle(30.0, 25.0);
        helpIcon.setFill(new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE,
                new Stop[]{
                    new Stop(0, Color.web("#4977A3")),
                    new Stop(0.5, Color.web("#B0C6DA")),
                    new Stop(1, Color.web("#9CB6CF")),}));
        helpIcon.setStroke(Color.web("#D0E6FA"));
        helpIcon.setArcHeight(3.5);
        helpIcon.setArcWidth(3.5);

        Text helpText = new Text("?");
        helpText.setFont(Font.font("Verdana", FontWeight.BOLD, 18));
        helpText.setFill(Color.WHITE);
        helpText.setStroke(Color.web("#7080A0"));

        stack.getChildren().addAll(helpIcon, helpText);
        stack.setAlignment(Pos.CENTER_RIGHT);     // Right-justify nodes in stack
        StackPane.setMargin(helpText, new Insets(0, 10, 0, 0)); // Center "?"

        hb.getChildren().add(stack);            // Add to HBox from Example 1-2
        HBox.setHgrow(stack, Priority.ALWAYS);    // Give stack any extra space
    }

    public GridPane addGridPane() {
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(0, 10, 0, 10));

        // Category in column 2, row 1
        Text category = new Text("Sales:");
        category.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(category, 1, 0);

        // Title in column 3, row 1
        Text chartTitle = new Text("Current Year");
        chartTitle.setFont(Font.font("Arial", FontWeight.BOLD, 20));
        grid.add(chartTitle, 2, 0);

        // Subtitle in columns 2-3, row 2
        Text chartSubtitle = new Text("Goods and Services");
        grid.add(chartSubtitle, 1, 1, 2, 1);

        // Left label in column 1 (bottom), row 3
        Text goodsPercent = new Text("Goods\n80%");
        GridPane.setValignment(goodsPercent, VPos.BOTTOM);
        grid.add(goodsPercent, 0, 2);

        // Right label in column 4 (top), row 3
        Text servicesPercent = new Text("Services\n20%");
        GridPane.setValignment(servicesPercent, VPos.TOP);
        grid.add(servicesPercent, 3, 2);

        return grid;
    }

    public FlowPane addFlowPane() {
        FlowPane flow = new FlowPane();
        flow.setPadding(new Insets(5, 0, 5, 0));
        flow.setVgap(4);
        flow.setHgap(4);
        flow.setPrefWrapLength(170); // preferred width allows for two columns
        flow.setStyle("-fx-background-color: DAE6F3;");

        return flow;
    }
}
