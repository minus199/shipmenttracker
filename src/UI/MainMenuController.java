/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Accordion;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
        

/**
 *
 * @author minus
 */
public class MainMenuController extends VBox {

    @FXML
    private Accordion pictureGallery;

    @FXML
    TableView<MainMenu.MetaInfoModel> metaInfoTable;

    @FXML
    private TableColumn<MainMenu.MetaInfoModel, String> metaName;

    @FXML
    private TableColumn<MainMenu.MetaInfoModel, String> metaValue;

    @FXML
    private ListView<String> pkgItemList;

    @FXML
    private AnchorPane test;

    @FXML
    private Parent root;

    @FXML
    public void initialize() {
        fillTable();
        assert pictureGallery != null;
        assert pkgItemList != null;
    }

    public void fillTable() {
        metaName.setCellValueFactory(new PropertyValueFactory<MainMenu.MetaInfoModel, String>("metaName"));
        metaValue.setCellValueFactory(new PropertyValueFactory<MainMenu.MetaInfoModel, String>("metaValue"));
    }

    public MainMenuController() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MainMenu2.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            System.out.println(exception.getMessage());
            throw new RuntimeException(exception);
        }
    }

    public Parent getRoot() {
        return root;
    }

    public void setRoot(Parent root) {
        this.root = root;
    }

    public Accordion getPictureGallery() {
        return pictureGallery;
    }

    public void setPictureGallery(Accordion pictureGallery) {
        this.pictureGallery = pictureGallery;
    }

    public ListView<String> getPkgItemList() {

        return pkgItemList;
    }

    public void setPkgItemList(ListView pkgItemList) {
        this.pkgItemList = pkgItemList;
    }

    public AnchorPane getTest() {
        return test;
    }

    public void setTest(AnchorPane test) {
        this.test = test;
    }

    public TableColumn<MainMenu.MetaInfoModel, String> getMetaName() {
        return metaName;
    }

    public void setMetaName(TableColumn<MainMenu.MetaInfoModel, String> metaName) {
        this.metaName = metaName;
    }

    public TableColumn<MainMenu.MetaInfoModel, String> getMetaValue() {
        return metaValue;
    }

    public void setMetaValue(TableColumn<MainMenu.MetaInfoModel, String> metaValue) {
        this.metaValue = metaValue;
    }

    public TableView<MainMenu.MetaInfoModel> getMetaInfoTable() {
        return metaInfoTable;
    }

    public void setMetaInfoTable(TableView<MainMenu.MetaInfoModel> metaInfoTable) {
        this.metaInfoTable = metaInfoTable;
    }
}
