/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Descriptors;

/**
 *
 * @author minus
 */
public class ShippingCostSummaryDescriptor {
    private final ConvertedCurrentPriceDescriptor priceDescriptor;
    private final String shippingType;

    public ShippingCostSummaryDescriptor(float amount, String currencyCode, String shippingType) {
        this.priceDescriptor = new ConvertedCurrentPriceDescriptor(currencyCode, amount);
        this.shippingType = shippingType;
    }

    public ConvertedCurrentPriceDescriptor getPriceDescriptor() {
        return priceDescriptor;
    }

    public String getShippingType() {
        return shippingType;
    }
}
