/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Descriptors;

import java.util.Currency;
import sun.util.resources.CurrencyNames;

/**
 *
 * @author minus
 */
public class ConvertedCurrentPriceDescriptor {
    private final String currency;
    private final Float amount;

    public ConvertedCurrentPriceDescriptor(String currency, Float amount) {
        this.currency = currency;
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public Float getAmount() {
        return amount;
    }
}
