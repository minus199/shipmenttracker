/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Descriptors;

/**
 *
 * @author minus
 */
public class ItemLocationDescriptor {
    private final String city;
    private final String country;

    public ItemLocationDescriptor(String city, String country) {
        this.city = city;
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }
}
