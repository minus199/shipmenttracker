/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _shippmenttracker;

import DB.ImageManager;
import DB.SQLiteJDBC;
import DB.TableCreator;
import UI.MainMenu;

import _shippmenttracker.eBayHandler.EBayItem;
import com.sun.org.apache.xalan.internal.xsltc.dom.CurrentNodeListFilter;
import com.sun.xml.internal.messaging.saaj.util.ByteInputStream;
import java.awt.Component;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTabbedPane;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.plaf.IconUIResource;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.fluent.Request;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpProtocolParams;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import sun.misc.IOUtils;

/**
 *
 * @author minus
 */
public class _ShippmentTracker {

    static String trackingNumber = "RS252281196NL";
    String courior = "postnl";

    private MainMenu mainMenu;

    public _ShippmentTracker() throws Exception {
        
        
        
        //this.mainMenu = new MainMenu();
    }

    public void db() {
        //        SQLiteJDBC sQLiteJDBC = new SQLiteJDBC();
        //        ImageManager imageManager = new ImageManager(sQLiteJDBC.getConnection());
        //        imageManager.addImageToDB("test", "http://i.ebayimg.com/00/s/MTIwMFgxMjAw/z/YRMAAOSwgQ9VgSWm/$_1.JPG?set_id=880000500F");
    }

    public static void main(String[] args) throws Exception {
//        Handler h = new Handler("261933387450");
//        System.out.println("startring");
        
        MainMenu.launch(args);
        MainMenu manager = new MainMenu();
        Stage stage = new Stage();
        manager.start(stage);
                
        
        
//        System.out.println(evt.getSource().getClass());
//        DefaultListModel defaultListModel = (DefaultListModel)getPackageList().getModel();
//        defaultListModel.addElement(evt.getActionCommand());
//        DefaultListModel listModel = new DefaultListModel();
//        listModel.addElement(trackingNumber);
//        JList list = mainMenu.getPackageList();
//        list.setModel(listModel);
//        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
//        list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
//        list.setVisibleRowCount(-1);
//        mainMenu.setTrackingNumber(trackingNumber);
        
//        System.out.println(new _ShippmentTracker().getIsraelPostData());
//        h.start();

    }

    public void startMenu() {
        System.out.println(getDescriptionTab().getComponent(index).getClass());
        Table table = mainMenu.getMetaTable();

        DefaultTableModel defaultTableModel = (DefaultTableModel) table.getModel();
        getItem().toArray().forEach((k, v) -> defaultTableModel.addRow(new Object[]{k, v}));

        table.setVisible(true);
        mainMenu.setPreferredSize(mainMenu.getPreferredSize());
    }

    public String getIsraelPostData() throws MalformedURLException, IOException {
        URL url = new URL("http://www.israelpost.co.il/itemtrace.nsf/trackandtraceJSON?openagent&_=1443179914946&lang=EN&itemcode=RS252281196NL&sKod2=");
        URLConnection connection = url.openConnection();

        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        StringBuilder response = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            response.append(line);
            response.append('\n');
        }
        br.close();

        JSONObject object = new JSONObject(response.toString());

        return object.getString("itemcodeinfo").split("<")[0];
    }

    public String doGet(URL url) throws IOException {
        try {
            HttpURLConnection connection;
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept", "application/xml");

            try {
                connection.setRequestMethod("GET");
            } catch (ProtocolException ex) {
                Logger.getLogger(_ShippmentTracker.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }

            /*for(Map.Entry<String,String> entry : getHeaders().entrySet()){
             connection.setRequestProperty(entry.getKey(), entry.getValue());
             }
            
             if (showHeaders){
             return connection.getHeaderFields().toString();
             }*/
            connection.setUseCaches(false);
            connection.setDoOutput(true);

            DataOutputStream dataOutputStream = new DataOutputStream(connection.getOutputStream());
            InputStream xml = connection.getInputStream();
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(xml);

            try {
                dataOutputStream.writeBytes(url.getQuery());
                dataOutputStream.flush();
                dataOutputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(_ShippmentTracker.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            StringBuilder response = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println("asdasd");
                response.append(line);
                response.append('\n');
            }
            br.close();

            return response.toString();
//        JSONObject object = new JSONObject(response.toString());
//        
//        return object.getString("itemcodeinfo").split("<")[0];
            //return response.toString();
        } catch (SAXException ex) {
            Logger.getLogger(_ShippmentTracker.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public AftershipDriver getAftershipDriver() {
        if (afterShipDriver instanceof AftershipDriver) {
            return afterShipDriver;
        }

        afterShipDriver = new AftershipDriver();
        return afterShipDriver;
    }

    public ArrayList<String> doTracking() {
        ArrayList<String> trackingRoute = new ArrayList<String>();

        ParametersTracking parametersTracking = new ParametersTracking();
        parametersTracking.addSlug(courior);

        for (Tracking tracking : getAftershipDriver().getConnection().getTrackings(parametersTracking)) {
            System.out.println(tracking.getOrderIDPath());
        }

        for (Checkpoint checkpoint : tracking.getCheckpoints()) {
            String message
                    = "Time: " + checkpoint.getCheckpointTime()
                    + "\nCity: " + checkpoint.getCity()
                    + "\nCountry: " + checkpoint.getCountryName()
                    + "\nMessage was: " + checkpoint.getMessage();

            trackingRoute.add(message);
        }

        return trackingRoute;
    }

    public void addTracking() {
        Tracking tracking = getAftershipDriver().addTracking(trackingNumber);
    }

    public static void doSomething(Node node) {
        System.out.println(node.getNodeName());

        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                //calls this method for all the children which is Element
                doSomething(currentNode);
            }
        }
    }

    public String getUrlParams(String itemCode, String vendor) {

        final List<NameValuePair> qParams = new ArrayList<NameValuePair>();

        switch (vendor) {
            case "eBay":
                qParams.add(new BasicNameValuePair("callname", "GetSingleItem"));
                qParams.add(new BasicNameValuePair("responseencoding", "XML"));
                qParams.add(new BasicNameValuePair("appid", eBayAppID));
                qParams.add(new BasicNameValuePair("siteid", "0"));
                qParams.add(new BasicNameValuePair("version", "515"));
                qParams.add(new BasicNameValuePair("ItemID", itemCode));
                qParams.add(new BasicNameValuePair("IncludeSelector", "Description,ItemSpecifics,ShippingCosts"));
                break;
            case "afterShip":
                qParams.add(new BasicNameValuePair("openagent", ""));
                qParams.add(new BasicNameValuePair("_", String.valueOf(Instant.now().getEpochSecond())));
                qParams.add(new BasicNameValuePair("lang", "EN"));
                qParams.add(new BasicNameValuePair("itemcode", itemCode));
                qParams.add(new BasicNameValuePair("sKod2", ""));
                break;
        }

        return URLEncodedUtils.format(qParams, "UTF-8");
    }

    public Map<String, String> getHeaders() {
        final Map<String, String> sendHeaders = new HashMap<String, String>();
        sendHeaders.put("Accept-Encoding", "gzip, deflate, sdch");
        sendHeaders.put("Accept-Language", "en-US,en;q=0.8,he;q=0.6");
        sendHeaders.put("Content-Type", "application/x-www-form-urlencoded");
        //sendHeaders.put("Referer", "http://www.israelpost.co.il/itemtrace.nsf/mainsearch?OpenForm&L=EN");
        sendHeaders.put("X-Requested-With", "XMLHttpRequest");
        sendHeaders.put("Content-Language", "en-US");
        sendHeaders.put("Connection", "keep-alive");

        return sendHeaders;
    }
}
