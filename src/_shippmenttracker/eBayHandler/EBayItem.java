package _shippmenttracker.eBayHandler;

import _shippmenttracker.Handler;
import Descriptors.ConvertedCurrentPriceDescriptor;
import Descriptors.ItemLocationDescriptor;
import Descriptors.ItemSpecificsDescriptor;
import Descriptors.ShippingCostSummaryDescriptor;
import UI.MainMenu;
import java.awt.Image;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Observable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableList;
import javafx.collections.ObservableListBase;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author minus
 */
public class EBayItem {

    private final String itemCode;
    private List<NameValuePair> qParams;
    private URL viewItemURLForNaturalSearch;
    private ItemLocationDescriptor location;
    private final List<URL> pictureURL;
    private String primaryCategoryName;
    private ConvertedCurrentPriceDescriptor convertedCurrentPrice;
    private String title;
    private ShippingCostSummaryDescriptor shippingCostSummaryDescriptor;
    private ItemSpecificsDescriptor itemSpecificsDescriptor;
    private String descriptionHTML;
    private NodeList xmlRes;

    private EBayItem(String itemCode) {
        //"261933387450";
        /*
         ItemSpecifics
         ShippingCostSummary
         Title
         ConvertedCurrentPrice
         PictureURL
         Location
         ListingType
         ViewItemURLForNaturalSearch
         ItemID
         Description
         */
        this.itemCode = itemCode;
        qParams = new ArrayList<NameValuePair>();
        qParams = getqParams();
        this.pictureURL = new ArrayList<URL>();
    }

    public static EBayItem factory(String itemCode)  {
        EBayItem eBayItem = new EBayItem(itemCode);

        try {
            return eBayItem.getItemXmlData().populate();
        } catch (IOException ex) {
            Logger.getLogger(EBayItem.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(EBayItem.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        } catch (SAXException ex) {
            Logger.getLogger(EBayItem.class.getName()).log(Level.SEVERE, null, ex.getMessage());
        }
        
        return null;
    }

    private EBayItem populate() {
        for (int index = 0; index < xmlRes.getLength(); index++) {
            if (index % 2 == 0) {
                continue;
            }
            Node currentNode = xmlRes.item(index);

            String city = "";
            String countryCode = "";

            switch (currentNode.getNodeName()) {
                case "ItemSpecifics":
                    extractItemSpecificsDescriptor(currentNode.getChildNodes());
                    break;
                case "ShippingCostSummary":
                    NodeList children = currentNode.getChildNodes();
                    shippingCostSummaryDescriptor = new ShippingCostSummaryDescriptor(
                            Float.parseFloat(children.item(1).getTextContent()),
                            children.item(5).getTextContent(),
                            children.item(3).getTextContent()
                    );

                    break;
                case "Title":
                    title = currentNode.getTextContent();
                    break;
                case "ConvertedCurrentPrice":
                    convertedCurrentPrice = new ConvertedCurrentPriceDescriptor(currentNode.getTextContent(), Float.parseFloat(currentNode.getTextContent()));
                    break;
                case "PictureURL":
                    try {
                        pictureURL.add(new URL(currentNode.getTextContent()));
                    } catch (MalformedURLException ex) {
                        Logger.getLogger(EBayItem.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    break;
                case "Location":
                    city = currentNode.getTextContent();
                    break;
                case "Country":
                    countryCode = currentNode.getTextContent();
                    break;
                case "ListingType":
                    break;
                case "ViewItemURLForNaturalSearch":
                    try {
                        viewItemURLForNaturalSearch = new URL(currentNode.getTextContent());
                    } catch (MalformedURLException ex) {
                        Logger.getLogger(EBayItem.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    break;
                case "ItemID":
                    break;
                case "Description":
                    descriptionHTML = currentNode.getTextContent();
                    break;
                case "PrimaryCategoryName":
                    primaryCategoryName = currentNode.getTextContent();
            }

            location = new ItemLocationDescriptor(city, countryCode);
        }

        return this;
    }
//
//    public void saveImageToGallery() {
//        url = new URL(currentNode.getTextContent());
//        Image image = ImageIO.read(url);
//
//        JLabel label = new JLabel();
//        label.setIcon(new ImageIcon(image));
//        label.setText("picture " + index);
//
//        pictureGallery.addTab("Picture " + index, label);
//    }

    private final EBayItem getItemXmlData() throws IOException, ParserConfigurationException, SAXException {
        URL url = new URL("http://open.api.ebay.com/shopping?" + getEncodedUrlParams());
        StreamSource xmlSource = new StreamSource(url.openStream());
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document xmlDocument = db.parse(xmlSource.getInputStream());

        Node response = xmlDocument.getElementsByTagName("GetSingleItemResponse").item(0);

        xmlRes = response.getChildNodes().item(9).getChildNodes();

        return this;
    }

    private ItemSpecificsDescriptor extractItemSpecificsDescriptor(NodeList currentNodes) {
        itemSpecificsDescriptor = new ItemSpecificsDescriptor();

        for (int index = 0; index < currentNodes.getLength(); index++) {
            NodeList nameValue = currentNodes.item(index).getChildNodes();
            if (nameValue.getLength() == 0) {
                continue;
            }

            itemSpecificsDescriptor.put(nameValue.item(0).getTextContent(), nameValue.item(1).getTextContent());
        }

        return itemSpecificsDescriptor;
    }

    public List<NameValuePair> getqParams() {
        if (qParams.isEmpty()) {
            qParams.add(new BasicNameValuePair("callname", "GetSingleItem"));
            qParams.add(new BasicNameValuePair("responseencoding", "XML"));
            qParams.add(new BasicNameValuePair("appid", Handler.geteBayAppID()));
            qParams.add(new BasicNameValuePair("siteid", "0"));
            qParams.add(new BasicNameValuePair("version", "515"));
            qParams.add(new BasicNameValuePair("ItemID", itemCode));
            qParams.add(new BasicNameValuePair("IncludeSelector", "Description,ItemSpecifics,ShippingCosts"));
        }

        return qParams;
    }

    public String getEncodedUrlParams() {
        return URLEncodedUtils.format(getqParams(), "UTF-8");
    }

    @Override
    public String toString() {
        return "EBayItem{" + "itemCode=" + itemCode +  ", viewItemURLForNaturalSearch=" + viewItemURLForNaturalSearch + ", location=" + location + ", pictureURL=" + pictureURL + ", primaryCategoryName=" + primaryCategoryName + ", convertedCurrentPrice=" + convertedCurrentPrice + ", title=" + title + ", shippingCostSummaryDescriptor=" + shippingCostSummaryDescriptor + ", itemSpecificsDescriptor=" + itemSpecificsDescriptor + ", xmlRes=" + xmlRes + '}';
    }
    
    public ArrayList<MainMenu.MetaInfoModel> toMetaInfoModels(){
        HashMap<String, String> objectContent = new HashMap<String, String>();
        
        objectContent.put("getItemCode", getItemCode());
        objectContent.put("title", getTitle());
        objectContent.put("categoryName", getPrimaryCategoryName());
        objectContent.put("urlForSearch", getViewItemURLForNaturalSearch().toString());
        objectContent.put("currentPrice.amount", getConvertedCurrentPrice().getAmount().toString());
        objectContent.put("currentPrice.currency", getConvertedCurrentPrice().getCurrency());
        objectContent.put("getItemCode", getItemCode());
        objectContent.put("location.city", getLocation().getCity());
        objectContent.put("location.country", getLocation().getCountry());
        objectContent.put("shippingType", getShippingCostSummaryDescriptor().getShippingType());
        objectContent.put("shippingCost.amount", getShippingCostSummaryDescriptor().getPriceDescriptor().getAmount().toString());
        objectContent.put("shippingCost.currency", getShippingCostSummaryDescriptor().getPriceDescriptor().getCurrency());

        ArrayList<MainMenu.MetaInfoModel> metaData = new ArrayList<>();
        for (Map.Entry<String, String> entrySet : objectContent.entrySet()) {
            metaData.add(new MainMenu.MetaInfoModel(entrySet.getKey(), entrySet.getValue()));
        }
        
        return metaData;
    }

    public String getItemCode() {
        return itemCode;
    }

    public URL getViewItemURLForNaturalSearch() {
        return viewItemURLForNaturalSearch;
    }

    public ItemLocationDescriptor getLocation() {
        return location;
    }

    public List<URL> getPictureURL() {
        return pictureURL;
    }

    public String getPrimaryCategoryName() {
        return primaryCategoryName;
    }

    public ConvertedCurrentPriceDescriptor getConvertedCurrentPrice() {
        return convertedCurrentPrice;
    }

    public String getTitle() {
        return title;
    }

    public ShippingCostSummaryDescriptor getShippingCostSummaryDescriptor() {
        return shippingCostSummaryDescriptor;
    }

    public ItemSpecificsDescriptor getItemSpecificsDescriptor() {
        return itemSpecificsDescriptor;
    }

    public String getDescriptionHTML() {
        return descriptionHTML;
    }
}
