/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _shippmenttracker;

import _shippmenttracker.eBayHandler.EBayItem;
import java.awt.Component;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.ImageView;
import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javafx.scene.image.Image;

/**
 *
 * @author minus
 */
public class Handler {

    final private static String eBayName = "shipmentTracker";
    final private static String eBayDEVID = "13660224-238f-4886-814d-547d80633e92";
    final private static String eBayAppID = "AsafBlum-fc49-4c50-a341-2a9f75be9f6b";
    final private static String eBayCertID = "1522c88a-efd3-47fa-84ee-0286526d57d8";
    private final String itemCode;
    private JTable table;

    private EBayItem eBayItem;

    public Handler(String itemCode) {
        this.itemCode = itemCode;
    }

    public void start() throws IOException {
    }

    public EBayItem getItem() {
        if (eBayItem instanceof EBayItem) {
            return eBayItem;
        }
        
        eBayItem = EBayItem.factory(itemCode);
        return eBayItem;
    }

    public ArrayList<java.awt.Image> getPictureGallery() throws IOException {
        ArrayList<java.awt.Image> data = new ArrayList<>();

        for (URL url : getItem().getPictureURL()) {
            java.awt.Image image = ImageIO.read(url);

            data.add(image);
        }

        return data;
    }

    public ArrayList<ImageView> getPictureGalleryAsImageView() throws IOException {
        System.out.println(getItem());
        ArrayList<ImageView> data = new ArrayList<>();

        for (URL url : getItem().getPictureURL()) {
            System.out.println(url.toString());
            InputStream imgInputStream = url.openStream();
            Image img = new Image(imgInputStream);
            data.add(new ImageView(img));
        }

        return data;
    }

    public static String geteBayName() {
        return eBayName;
    }

    public static String geteBayDEVID() {
        return eBayDEVID;
    }

    public static String geteBayAppID() {
        return eBayAppID;
    }

    public static String geteBayCertID() {
        return eBayCertID;
    }
}
