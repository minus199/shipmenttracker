/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _shippmenttracker;

import Classes.AftershipAPIException;
import Classes.ConnectionAPI;
import Classes.Courier;
import Classes.Tracking;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

/**
 *
 * @author minus
 */
public class AftershipDriver {
    private List<Courier> couriers;
    
    public ConnectionAPI getConnection(){
        return new ConnectionAPI("e1309c81-c259-4a57-b658-51cabc9e8a83");
    }
    
    public List<Courier> getCouriors(){
        if (!couriers.isEmpty())
            return couriers;

        try {
            couriers = getConnection().getAllCouriers();
            for(int i=0;i<couriers.size();i++){
                System.out.println(couriers.get(i).getSlug());
            }
        } catch (AftershipAPIException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return couriers;
    }
    
    public List<NameValuePair> getHeaders(){
        final List<NameValuePair> headers=new ArrayList<NameValuePair>();
        
        headers.add(new BasicNameValuePair("aftership-api-key", "e1309c81-c259-4a57-b658-51cabc9e8a83"));
        headers.add(new BasicNameValuePair("Content-Type", "application/json"));

        return headers;
    }
    
    public String getUrlParams(String itemCode){
        
        final List<NameValuePair> qParams=new ArrayList<NameValuePair>();
        
        qParams.add(new BasicNameValuePair("openagent", ""));
        
        qParams.add(new BasicNameValuePair("_", String.valueOf(Instant.now().getEpochSecond())));
        qParams.add(new BasicNameValuePair("lang", "EN"));
        qParams.add(new BasicNameValuePair("itemcode", itemCode));
        qParams.add(new BasicNameValuePair("sKod2", ""));
                
        return URLEncodedUtils.format(qParams, "UTF-8");
    }
    
    public List<Courier> getAccountCouriors(String trackingNumber){
        try {
            return getConnection().detectCouriers(trackingNumber);
        } catch (AftershipAPIException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public Tracking trackItem(String trackingNumber, String courior){
        try {
            Tracking trackingToGet = new Tracking(trackingNumber);
            trackingToGet.setSlug(courior);
            
            return getConnection().getTrackingByNumber(trackingToGet);
        } catch (AftershipAPIException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
    
    public Tracking addTracking(String trackingNumber){
        try {
            Tracking tracking = new Tracking(trackingNumber);
            
            //Then we can add information;
            tracking.setSlug("dpd");
            tracking.setTitle("this title");
            tracking.addEmails("email@yourdomain.com");
            tracking.addEmails("another_email@yourdomain.com");
            tracking.addSmses("+85292345678");
            tracking.addSmses("+85292345679");
            
            
            //Even add customer fields
            tracking.addCustomFields("product_name","iPhone Case");
            tracking.addCustomFields("product_price","USD19.99");
            
            //Finally we add the tracking to our account
            return getConnection().postTracking(tracking);
        } catch (AftershipAPIException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(AftershipDriver.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return null;
    }
}
