/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ShippingPackage;

import _shippmenttracker.eBayHandler.EBayItem;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author minus
 */
public class Package {
    private final EBayItem eBayItem;
    private final String trackingNumber;
    private final int packageID;

    public Package(String trackingNumber, URL itemURL) {
        this.trackingNumber = trackingNumber;
        this.packageID = 0;
        
        System.out.println(itemURL.getHost().split("."));
        eBayItem = EBayItem.factory(trackingNumber);
    }
    
    public Package(Integer packageID) {
        this.trackingNumber = null;
        this.packageID = packageID;
        eBayItem = EBayItem.factory(trackingNumber);
    }
    
    public void save(){
        
    }

    public EBayItem geteBayItem() {
        return eBayItem;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }
    
    public String VendorType(){
        return "eBay";
    }
}
