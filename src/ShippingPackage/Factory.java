/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ShippingPackage;

/**
 *
 * @author minus
 */
public class Factory {
    public static Package get(String trackingNumber){
        return new Package(trackingNumber);
    }
    
    public static Package get(Integer id){
        return new Package(id);
    }
}
