/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import _shippmenttracker.eBayHandler.EBayItem;
import com.sun.jna.platform.FileUtils;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.net.URI;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

/**
 *
 * @author minus
 */
public class ImageManager {
    private final Connection connection;
    private final EBayItem eBayItem;

    public ImageManager(Connection connection, EBayItem eBayItem) {
        this.connection = connection;
        this.eBayItem = eBayItem;
    }
    
    public ArrayList<Image> getGalleryImages(){
        ArrayList<Image> images = new ArrayList<Image>();
        // find all images for this item in db and prepare array of images 
        return images;
    }

    private byte[] getByteArrayFromFile(String filePath) {
        byte[] result = null;
        FileInputStream fileInStr = null;
        try {
            //Image image = ImageIO.read(new URL(filePath));
            
            URL url = new URL(filePath);
            BufferedImage img = ImageIO.read(url);
            File imgFile = new File("downloaded.jpg");
            ImageIO.write(img, "jpg", imgFile);
            
            fileInStr = new FileInputStream(imgFile);
            long imageSize = imgFile.length();

            if (imageSize > Integer.MAX_VALUE) {
                return null;    //image is too large  
            }

            if (imageSize > 0) {
                result = new byte[(int) imageSize];
                fileInStr.read(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fileInStr.close();
            } catch (Exception e) {
            }
        }
        return result;
    }

    public void addImageToDB(String name, String imageName) {
        String query = "INSERT INTO pictures2(id,name,img_content) VALUES (?, ?, ?)";
        PreparedStatement prepStmt = null;
        try {
            connection.setAutoCommit(false);
            prepStmt = connection.prepareStatement(query);
            
            prepStmt.setString(1, "1");
            
            prepStmt.setString(2, name);

            byte[] imageFileArr = getByteArrayFromFile(imageName);
            prepStmt.setBytes(3, imageFileArr);

            prepStmt.executeUpdate();
            connection.commit();
            JOptionPane.showMessageDialog(null, "Image saved successfully!", "Successfull", JOptionPane.INFORMATION_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                prepStmt.close();
            } catch (Exception e) {
            }
        }
    }

    public Image getImage(String name) {
        Image img = null;
        String query = "select image from pictures2 where id=1";
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            ResultSet rslt = stmt.executeQuery(query);
            if (rslt.next()) {
                byte[] imgArr = rslt.getBytes("image");
                img = Toolkit.getDefaultToolkit().createImage(imgArr);
            }
            rslt.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                connection.close();
                stmt.close();
            } catch (Exception e) {
            }
        }

        return img;
    }
}
