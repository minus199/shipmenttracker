/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

/**
 *
 * @author minus
 */
public class TableCreator {

    public TableCreator(Connection connection) {
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            
            String sql;
            sql = "CREATE TABLE PICTURES"
                    + "(ID INT PRIMARY KEY NOT NULL,"
                    + " NAME TEXT NULL, "
                    + " CONTENT        BLOB    NOT NULL, "
                    + " DATE_CREATED   TIMESTAMP DEFAULT NOW())";
            sql = "CREATE TABLE PICTURES2 " +
                   "(ID INT PRIMARY KEY     NOT NULL," +
                   " NAME           TEXT    NOT NULL, " + 
                   " IMG_CONTENT            BLOB     NOT NULL)";
            
            System.out.println(sql);
            stmt.executeUpdate(sql);
            stmt.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

        System.out.println("Table created successfully");
    }
}
